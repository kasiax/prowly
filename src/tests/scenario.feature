Feature: Prowly filtering

  Scenario: "Prowly" Basic Search
    Given I have navigated to google.com.
    When I enter "prowly" into the search bar and press 'Enter'.
    Then I should see a search results page with relevant links related to "prowly", including the main website for Prowly, related articles, blog posts, reviews, and other content.

  Scenario "Prowly" Video Search
    Given I have navigated to google.com and have already searched for "prowly".
    When I click on the 'Videos' tab below the search bar.
    Then I should see a search results page displaying only video content related to "prowly" from sources like YouTube, Vimeo, and other video hosting platforms.

  Scenario: "Prowly" News Search
    Given I have navigated to google.com and have already searched for "prowly".
    When I click on the 'News' tab below the search bar.
    Then I should see a search results page displaying recent news articles related to "prowly" from trusted news outlets.
